## Setup
`$ yarn`

## Update config
```
#!javascript
// config.js
{
    feature: {
        feature_file: '', // name of the official feature file (the one that is being required by the app)
        use: ''           // rename this file to whatever is specified by config.feature_file
    }
}

```

## Run
`$ gulp`

This will create the `dist/`.
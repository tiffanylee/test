// config.js and gulpfile.js should be in the root!
// currently lib is hard coded
(function() {

    "use strict";

    var gulp    = require('gulp'),
        rename  = require('gulp-rename'),
        ignore  = require('gulp-ignore'),
        _       = require('lodash'),
        nodePath= require('path'),
        match   = require('gulp-match');

    gulp.task('default', function() {

        var config  = require('./config.js');
        var features= _.values(config);

        var hash_of_files_to_changes = processConfig(config);

        var list_of_files_to_remove = _.reduce(hash_of_files_to_changes, function(mem, value, key) {

            value = _.tail(value.split('/')).join('/');

            mem.push(value);

            return mem;

        }, []);

        // vars
        console.log('files to remove:', list_of_files_to_remove);
        console.log('hash_of_files_to_changes:', hash_of_files_to_changes);

        gulp.src('./lib/**/*.js')
            .pipe(ignore.exclude(function(file) {
                console.log(match(file, list_of_files_to_remove));
                // remove lib from path

                return match(file, list_of_files_to_remove);
            }))
            .pipe(rename(function(path) {

                var combined_path = nodePath.normalize(['./lib/', path.dirname, path.basename + path.extname].join('/'));
                var new_file_name = _.get(hash_of_files_to_changes, combined_path, '');

                if (new_file_name) {
                    path.basename = nodePath.basename(new_file_name).split('.')[0];
                }

            }))
            .pipe(gulp.dest('./dist'));




        // Helper Functions
        function processConfig(config) {

            var hash_of_files_to_change = _.reduce(features, function(mem, value) {

                var from = nodePath.normalize(_.get(value, 'from', '')),
                    to   = nodePath.normalize(_.get(value, 'to', ''));

                if (from) {
                    mem[from] = to;
                }

                return mem;

            }, Object(null));

            return hash_of_files_to_change;
        }

    });


})();
